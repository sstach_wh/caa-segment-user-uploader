const axios = require('axios'),
    readline = require('readline'),
    fs = require('fs');

const segmentName = 'soft_slots'; 
const fileName = 'data21-Odds_soft_slots.csv';
const envName = 'nonprod';
/**
 * you should define what segments will be added here
 * @type {{(segment_name: string): [string]}}
 */
const data = {
    [segmentName]: []
}

/**
 * Starting time of application to measure time
 * @type {number}
 */
const startTime = Date.now();

/**
 * Function to read data from file and push it to predefined array of strings in object data
 * @param name file name
 * @returns {Promise<void>}
 */
async function getFile(name) {
    const fileStream = fs.createReadStream(name);

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    for await (const line of rl) {
        const spliced = line.split(";")
        /**
         * By default data is pushed to 'promotion' key
         * You can change name of the segment pushed here
         *
         * if csv file is in format of USER_ID;segment_name
         * you can you spliced[1].trim() instead of 'promotion'
         */
        data[segmentName].push(spliced[0].trim());
    }
}

(async function () {
    /**
     * You are defining what files should be read at the beginning of the script
     */
    await getFile(fileName);
    // await getFile('dataB.csv');

    /**
     * You are creating array of promises that pushes segment's with array of users_ids to CAA
     */
    const arrProd = Object
        .entries(data)
        .map(
            ([seg, users]) => {
                return () => axios.post(`http://customer-activity.gaming.aws-eu-west-1.${envName}.williamhill.plc/segments/${seg}`, {accountIds: users})
            }
        )

    /**
     * just logging how many seconds script is working
     */
    const ix = setInterval(() => {
        console.log(((Date.now() - startTime) / 1000).toFixed(0))
    }, 10000)

    /**
     * you are lunching promises
     */
    arrProd.forEach((item) => {
        item().then(res => {
            console.log('success', (Date.now() - startTime) / 1000)
        }).catch(e => {
            console.log(e.response.data);
            console.error(e.message)
            console.log(e.data);
        }).finally(_ => {
            clearInterval(ix);
        })
    })

})()
